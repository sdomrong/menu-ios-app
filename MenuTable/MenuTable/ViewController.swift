//
//  ViewController.swift
//  menutable
//
//  Created by Sith Domrongkitchaiporn on 2/25/16.
//  Copyright © 2016 Sith Domrongkitchaiporn. All rights reserved.
//

import UIKit

struct MenuItem {
    var name: String
    var shortDescription: String
    var longDescription: String
    var image: UIImage?
    var map: UIImage?
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    
    let items = [
        MenuItem(name: "Parmesan Risotto", shortDescription: "Wild mushroom, Sweet Pumpkin, sprikled bread crumbs", longDescription: "We provide our customers with only the best rice.  After all, it is the rice that makes the dish.  Thailand's Jasmine rice is one of the best in the world; it offers a wonderful grain that can’t be offered by other varieties of rice. We offer a side of wild mixed mushrooms imported from the forests of Japan. Additionally, sweet organic pumpkin grown locally in California farms provides a bold flavor to an Italian dish. Bread crumbs and Parmesan are sprinkled on top generously.", image: UIImage(named: "risotto"), map: UIImage(named: "risotto_map")),
        
        MenuItem(name: "Seared Diver Scallop", shortDescription: "Heritage apple, walnut and rocket leaves", longDescription: "The scallops are harvested off the bay of Nova Scotia, by divers who handpick mature ones and leave young scallops to grow and replenish. These pan seared scallops have a perfectly brown crust and a flavorful seasoning blend. The ripe, juicy heritage apple and dark walnut mix provides a delightful contrast and will leave you enchanted.", image: UIImage(named: "scallops"), map: UIImage(named: "scallops_map")),
        
        MenuItem(name: "Prime Ribeye Steak", shortDescription: "Red wine demi glazed, and season vegetables", longDescription: "Organically raised in Selma, California, this ribeye is a cut of gorgeous meat with amazing juices.  These cows were raised according to nutrition standards well above requirements.  We use a French technique called Souvide that cooks your meat at a constant temperature and it may be the first time you will see the characteristic tender pink-red slice of steak from one side to the other.  To top it all off, we have a red wine glaze that enhances and brings the dish together. A side of seasonal spring vegetables may include cherry tomatoes, roasted asparagus, alta brussel sprouts with creme fraiche, and more.", image: UIImage(named: "steak"), map: UIImage(named: "steak_map")),
        
        MenuItem(name: "Norwegian Wild Salmon", shortDescription: "Yogurt dill sauce, steamed spinach", longDescription: "The North Atlantic Salmon is a species known for its fighting spirit.  These Norwegian Salmon are fetched from the city Larvik which boasts a salmon rich river in its cold, freshwater eddies, as well as a long coastline.  The salmon is fried on a searing hot pan for a crispy skin, with the meat still slightly pink.  The smooth greek yoghurt sauce with the sharp dill accent combined with the fresh, fatty salmon gives a rich taste. Lightly steamed spinach finishes off the dish with its light, fluffy texture.", image: UIImage(named: "salmon"), map: UIImage(named: "salmon_map")),
        
        MenuItem(name: "Grilled Chicken Breast", shortDescription: "Fried lemon thyme, carramelized homegrown vegetables", longDescription: "We are blessed to have chicken meat provided to us from Maust's California Poultry.   The chicken is grilled with smoky oil for maximum taste.  The fried lemon thyme adds a soft citrus smell with a tangy feel to the dish. This is a house recipe that was handed down from generation to generation. Our homegrown veggies include wild onions, mushrooms, and beans, lending an earthy taste that brings you back to your ancestor’s farming roots.", image: UIImage(named: "chicken"), map: UIImage(named: "chicken_map")),
        
        MenuItem(name: "Creme Brulee", shortDescription: "Mint-Berries", longDescription: "Creme Brulee, or “burnt cream” in French, is known all around the world for its characteristic vanilla custard and caramel on top.  (Whether french or not is hotly debated.)  You will simply have a feeling of pure joy as you break the top with a teaspoon.  The usage of the vanilla bean offers an enhanced vanilla flavor which cannot be obtained by using vanilla extract. As with all our dishes that contain poultry, the free-range eggs we use are from Maust's California Poultry.  We put a twist on this famous dish with some added Cinnamon and berries topped with fresh mint.", image: UIImage(named: "cremebrulee"), map: UIImage(named: "chicken_map")),
        
        MenuItem(name: "Lemon Parfait", shortDescription: "Honey, Bergamot, natural yogurt", longDescription: "The highlight here is the Bermagot orange from Maricopa County, Arizona.  Its fragrance is well known and is used in more than 2/3 of the women’s perfume. As taste and smell are closely related, the usage of the Bermagot orange will be sure to please your tastebuds. The orange is brilliantly paired with golden honey and the natural greek yogurt.  Share with your family.  It is definitely a dessert to try.", image: UIImage(named: "parfait"), map: UIImage(named: "parfait_map")),
        
        MenuItem(name: "Lithium Polymer Cell", shortDescription: "Charged Lithium Ion, polyethylene oxide", longDescription: "FOR CYBORGS AND ROBOTS: Advertised as the world's longest lasting battery for high tech devices.  Made in the K2 factory (Jiangsu, China), it has guaranteed leakage protection to protect you while you use it. These K2 cells are known to sustain the high end components you need.  Capacity: = 2.6 Ah, Voltage: 3.2V, Energy density: 100Wh,kg. Warning: This battery is not rechargable and attempts can result in combustion.", image: UIImage(named: "k2"), map: UIImage(named: "K2_map")),
        
        MenuItem(name: "Alkali Cell", shortDescription: "Zinc Manganese(IV) oxide", longDescription: "FOR CYBORGS AND ROBOTS: GP batteries have been around for years and produces one of the best alkali batteries.  Originating in Hong Kong, they now they have factories all across Asia. Of course, we get ours from the the original site.  These cells are extremely reliable and rechargeable.  At this point, we only serve the trusted 1.5V version, but higher voltage batteries are coming soon.", image: UIImage(named: "alkali"), map: UIImage(named: "alkali_map")),
        
        MenuItem(name: "Least Laxity Charging", shortDescription: "Slack time schedule algorithm", longDescription: "FOR ROBOTS: Charging programming allows you to charge up fully by predicting when you are leaving and trying to get you charged by then. The slow ramp up in current is a soothing feeling that one should try. Our powerbanks provide more than power for all usage. The personal touch to this programming allows you to relax and not worry about leaving uncharged and dying in the middle of whatever you are doing. Please contact the waiters for more details.", image: UIImage(named:"LLFCharge"), map: UIImage(named: "llf")),
        
        MenuItem(name: "Online LP Charging", shortDescription: "Complex Linear Programming for maximize charging", longDescription: "FOR ROBOTS: A Matrix calculates the best possible charging for you. It maximizes the charge you receive by connecting to a central server located in Pasadena, California, and sharing the maximum charge for all that is connected. Please contact a waiter for more details and availability. Due to the extremely fast charging, this is not recommended for elders with severe battery issues. Your safety is our priority.", image: UIImage(named:"LPCharge"), map: UIImage(named: "LP-1")),
        
        MenuItem(name: "Tropical Forrest Waterfall - USB", shortDescription: "Experience the fresh cool of being near a waterfall", longDescription: "FOR ROBOTS: USB experience allows the user to immerse himself/herself in a different environment that we consider refreshing.  Just plug in and you will start to feel as if you are in a different environment. Allow yourself to fall into the depths of the cool, refreshing feelings. We have the waterfall experience captured from the mountains of Thailand. Allow yourself to smell the fresh air, and feel the fresh cool breeze as well as hear the outdoors. You will be one with nature.  Understand why people enjoy a waterfall breeze so much", image: UIImage(named: "waterfall"), map: UIImage(named: "waterfall_map")),
        
        MenuItem(name: "Hua Hin Shores - USB", shortDescription: "Experience the ocean breze", longDescription: "FOR ROBOTS: USB experience allows the user to immerse himself/herself in a different environment that we consider refreshing.  Just plug in and you will start to feel as if you are in a different environment. Allow yourself to fall into the depths of feelings. We have the beach experience captured from the Hua Hin Beach of Thailand. Allow yourself to smell the salty beach air, feel the cool breeze as well as hear the soothing waves rushing into the shores. The sun beating down upon you is sure to relax you.", image: UIImage(named: "beach"), map: UIImage(named: "beach_map"))
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableview.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCell
        
        cell.photo.image = items[indexPath.row].image
        cell.Name.text = items[indexPath.row].name
        cell.Description.text = items[indexPath.row].shortDescription
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier! {
        case "itemTap":
            let cell = sender as! UITableViewCell
            let i = tableview.indexPathForCell(cell)!.row
            let vc = segue.destinationViewController as! MenuItemController
            vc.item = items[i]
            
        default:
            fatalError("Uneidfjadslkfjasd")
        }
    }

}

