//
//  CustomCell.swift
//  menutable
//
//  Created by Sith Domrongkitchaiporn on 2/25/16.
//  Copyright © 2016 Sith Domrongkitchaiporn. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
   
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Description: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
