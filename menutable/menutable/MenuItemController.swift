//
//  MenuItemController.swift
//  menutable
//
//  Created by Sith Domrongkitchaiporn on 2/25/16.
//  Copyright © 2016 Sith Domrongkitchaiporn. All rights reserved.
//

import UIKit

class MenuItemController: UIViewController {

    var item: MenuItem! {
        didSet {
            view.setNeedsLayout()
            view.layoutIfNeeded()
        }
    }
        
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var map: UIImageView!
    @IBOutlet weak var long: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        name.text = item.name
        long.text = item.longDescription
        photo.image = item.image
        map.image = item.map
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
